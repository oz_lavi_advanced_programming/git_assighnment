#include "Vector.h"


Vector::Vector(int n)
{
	if (n < 2)
	{
		n = 2;
	}
	_capacity = n;
	_elements = new int[n];
	_size = 0;
	_resizeFactor = n;
}
Vector::~Vector()
{
	delete[] _temp;
	_temp = NULL;
	delete[] _elements;
	_elements = NULL;
}
int Vector::size()const
{
	return _size;
}
int Vector::capacity() const
{
	return _capacity;
}
int Vector::resizeFactor()const
{
	return _resizeFactor;
}
bool Vector::empty() const
{
	if (_size == 0)
	{
		return true;
	}
	else
	{
		return false;
	}
}
void Vector::push_back(const int& val)
{
	if (_size < _capacity)
	{
		_elements[_size] = val;
		_size++;
	}
	else if (_size == _capacity)
	{
		int i = 0;
		_temp = _elements;
		_elements = new int[_capacity + _resizeFactor];
		for (i = 0; i < _capacity; i++)
		{
			_elements[i] = _temp[i];
		}
		delete[] _temp;
		_elements[_size] = val;
		_size++;
		_capacity+=_resizeFactor;
	}
}
int Vector::pop_back()
{
	int returnValue = 0;
	if (_size == 0)
	{
		std::cout << "error: pop from empty vector" << std::endl;
		_size--;
		return -9999;
	}
	else
	{
		 
		returnValue = _elements[_size - 1];
		_elements[_size - 1] = 0;
		_size--;
		return returnValue;
	}

}
void Vector::reserve(int n)
{
	int i = 0;
	if (_capacity <= n)
	{
		while (_capacity < n)
		{
			_capacity += _resizeFactor;
		}
		_temp = _elements;
		_elements = new int[_capacity];
		for (i = 0; i < _capacity; i++)
		{
			_elements[i] = _temp[i];
		}
		delete[] _temp;
	}
}
void Vector::resize(int n)
{
	int i = 0;
	if (n == _capacity)
	{
		return;
	}
	else if (n < _capacity)
	{
		_temp = _elements;
		_elements = new int[n];
		for (i = 0; i < n; i++)
		{
			_elements[i] = _temp[i];
		}
		delete[] _temp;
	}
	else if (_capacity < n)
	{
		while (_capacity < n)
		{
			_capacity += _resizeFactor;
		}
		_temp = _elements;
		_elements = new int[_capacity];
		for (i = 0; i < _capacity; i++)
		{
			_elements[i] = _temp[i];
		}
		delete[] _temp;
	}
	_size = n;
	_capacity = n;
}
void Vector::assign(int val)
{
	int i;
	for (i = 0; i < _capacity; i++)
	{
		_elements[i] = val;
	}
}
void Vector::resize(int n, const int& val)
{

	int i;
	if (n == _capacity)
	{
		return;
	}
	else if (n < _capacity)
	{
		_temp = _elements;
		_elements = new int[n];
		for (i = 0; i < n; i++)
		{
			_elements[i] = _temp[i];
		}
		delete[] _temp;
	}
	else if (_capacity < n)
	{
		while (_capacity < n)
		{
			_capacity += _resizeFactor;
		}
		_temp = _elements;
		_elements = new int[_capacity];
		for (i = 0; i < _capacity; i++)
		{
			_elements[i] = _temp[i];
		}
		delete[] _temp;
	}
	_size = n;
	_capacity = n;
	for (i = 0; i < _capacity; i++)
	{
		_elements[i] = val;
	}
}
//i think that i don't need them in the program


int& Vector::operator[](int n) const
{
	return _elements[n];
}